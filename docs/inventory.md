# Inventory

Searching a medicine from the global database to impliment it in the search field.
## Search types
1. Searching with First letters algorithm.
2. Will search with substring.

Search creates a list of two above. Then one is selected and a form is opened.

## Form
* Price
  1. Standard price
  2. Customised price(Can be modified by the user)
  3. Custom Price by percentage
* Discount
  1. Standard price
  2. Customised price(Can be modified by the user)
  3. Custom Price by percentage
* Tax
  1. Standard price
  2. Customised price(Can be modified by the user)
  3. Custom Price by percentage

** Note: Additional changes on Tax to be finalized **
