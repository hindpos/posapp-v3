# Bill
1. New order
2. Refund order
3. Discard order
4. Due order
5. Refund and Discard

| List | Income | Stock|
| ---- | :----: | -----|
| New order | ↑ | ↓|
| Refund order | ↓ |↑|
| Discard order |constant | ↓|
| Due order | ↑| constant |
| Refund and discard order | ↓ | ↓ |

### New order
* Here the products are searched accordingly as per the frequency of used medicines and suggestions would be provided in the search bar from where the medicines could be added to the cart.
* The standard rate would be provided and editable options would also be available.

### Refund order
* Here the previous bill is looked up and accordingly the bill is generated.

### Discard order
*  Here the products are searched accordingly as per the frequency of used medicines and suggestions would be provided in the search bar from where the medicines could be added to the cart.
* The price is set to 0.

### Due order
* Here the previous due bill is looked up and then it is modified and the final bill is released.

### Refund and Discard order
* Here the previous bill is looked up and bill is generated along with the decrease in Income and Stock.
