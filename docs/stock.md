# Stock
## Stock History
List of Stock objects numbered by time-stamp

Example

** Stock 1 **

* Distributer : Sayan Medical Pvt. Ltd
* Time Stamp: 11:30 PM 22nd December 2017
* Extra Info

| Product | Count | Price | Total Price|
| ------- |:-----:|:-----:| ----------:|
| Paracitamol  | 10  | 100| 1000|
| Rantac  | 5  | 50| 250|
||||1250|

** Stock 1 **

* Distributer : Sayan Medical Pvt. Ltd
* Time Stamp: 12:30 PM 31st December 2017
* Extra Info

| Product | Count | Price | Total Price|
| ------- |:-----:|:-----:| ----------:|
| Paracitamol  | 5  | 150| 750|
| Rantac  | 5  | 75 | 375|
||||1125|


## Create Stock

Form to create Stock

* Distributer
* Extra Info
* Time (Auto-generated)
* Product List
  * Product (Search-able)
  * Count
  * Price
  * Totals (Auto-Calculated)

## Current Stock
Normal View

| Product | Count | Price | Total Price|
| ------- |:-----:|:-----:| ----------:|
|Total|
| Paracitamol  | 15  | 117 (Weighted Avg. Price) | 1750 |
| Rantac  | 10  | 62.5 (Weighted Avg. Price) | 625|
||||1250|


Expanded View

| Product | Count | Price | Total Price|
| ------- |:-----:|:-----:| ----------:|
|**Stock 1**|
| Paracitamol  | 10  | 100| 1000|
| Rantac  | 5  | 50| 250|
||||1250|
|**Stock 2**|
| Paracitamol  | 5  | 150| 750|
| Rantac  | 5  | 75| 375|
||||1250|
|**Total**|
| Paracitamol  | 15  | 117 (Weighted Avg. Price) | 1750|
| Rantac  | 10  | 62.5 (Weighted Avg. Price) | 625|
||||1250|
