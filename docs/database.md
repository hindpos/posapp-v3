### Transactions
* Transaction ID (Autogen)
* Type
  * Order
  * Stock
* Order/Stock Id
* Buyer ID (Store ID if stock)
* Seller ID (Store ID if order)
* TimeStamp

### Order
* Order ID (Autogen)
* Attach Order ID
* Transaction ID
* TimeStamp
* Type
  1. New order
  2. Refund order
  3. Discard order
  4. Due order
  5. Refund and Discard
* Product List
  * Product ID
  * Price
  * Tax
  * Discount
  * Qty
  * Total
* Total Price
* Tax
* Discount


### Product
* Name
* Stockable (Bool)
* Product ID (Autogen)
* Database ID
* Price
  * Database
  * Custom on Database
    * Percent
    * Amount
  * Custom
  * On Last Stock  
* Tax
  Dict
  * Tax type
  * Percent
  * Amount
* Discount
  * Percent
  * Amount
* ReOrder Quantity
* Type
  * Medicine
  * Grocery
  * Service
  * Food
  * Other
* Details (dependent on type)

### Stock
* Stock Id (Autogen)
* Stock Num
* TimeStamp
* Taxes
* Price
* Total
* Product List
  * Stock Product Id (Given)
  * Product Id
  * Details
  * Expiry
  * Taxes
  * Price
  * Qty
  * Total


### Current State
* Stock List
  * Id
  * Product List Current
    * Product Id
    * Num
* Total Stock
  * Product Id
  * Product Num
  * Weighted Stuff

<b>** See stock.md</b>
