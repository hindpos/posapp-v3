 # Order History

List of Orders can be **Sorted** and **Filtered** by:

* Time
* Price
* User
* Customer

Selecting a order opens a new page with order details and abilities to link with new orders.

### Order Details Page

* Customer Info
* Payment Method Info
* Time
* User
* Product List

|Product| Original Price|	Price	|Quantity|	Tax Amount|	Tax Percent|	Discount Amount|	Total|
|---:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|
|Product 1|		345.50₹|	345.50₹|	2	|2.00₹|	4%|	0.00₹|	691.00₹|
|Product 1|		345.50₹|	345.50₹|	2	|2.00₹|	4%|	0.00₹|	691.00₹|
|Product 1|		345.50₹|	345.50₹|	2	|2.00₹|	4%|	0.00₹|	691.00₹|
|Product 1|		345.50₹|	345.50₹|	2	|2.00₹|	4%	|0.00₹|	691.00₹|

* Total Payment Info

|Sub Total| ₹1,124.50|
|------:|:--|
|Total Taxes| ₹45.50|
|Discount| ₹00.00|
|Grand Total| ₹1,260.00|
|Total Paid| ₹1,000.00|
|Total Refunded| ₹0.00|
|Total Due| ₹260.50|
