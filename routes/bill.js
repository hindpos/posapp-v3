var express = require('express');
var router = express.Router();

// GET bill page
router.get('/', function(req, res, next) {
  res.render('bill', { title: 'Bill' });
});

module.exports = router;
