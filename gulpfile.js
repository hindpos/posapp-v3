const gulp = require('gulp');
const uglify = require('gulp-uglify');
// const livereload = require('gulp-livereload');
const concat = require('gulp-concat');
const minifyCss = require('gulp-minify-css');
const autoPrefixer = require('gulp-autoprefixer');
const plumber = require('gulp-plumber');
const sass = require('gulp-sass');
// const sourcemaps = require('gulp-sourcemaps');
const imageMin = require('gulp-imagemin');
const eslint = require('gulp-eslint');
const babel = require('gulp-babel');
const rename = require('gulp-rename');
const order = require('gulp-order');
var browserSync = require('browser-sync');
var nodemon = require('gulp-nodemon');
const fs = require('fs');

//File PATH of source
const scriptsPath = 'src/js/**/*.js';
const imagePath = 'src/img/**/*.{png,svg,jpeg,jpg,gif}';
const fontPath = 'src/fonts/**/*.{ttf,otf,woff,eot,svg}';
const cssPath = 'src/css/**/*.css';
const scssPath = 'src/scss/**/*.scss';
const scssFile = 'src/scss/main.scss';
const finalCssPath = ['src/.tempCSS/**/*.css', 'src/.tempSCSS/**/*.css'];
const orderFile = 'src/js/order.json';

//File PATH of destination
const DistPathOfCss = 'src/.tempCSS';
const DistPathOfScss = 'src/.tempSCSS';
const DistPathOfJs = 'public/js';
const DistPathOfImage = 'public/img';
const DistPathOfFont = 'public/font';

//Style for CSS
gulp.task('css', () => {
    return gulp.src(cssPath)
        .pipe(plumber((err) => {
            console.log(`CSS task error: ${err.name}`);
            console.log(err.message);
        }))
        .pipe(autoPrefixer())
        // .pipe(sourcemaps.init())
        .pipe(concat('tempCss.css'));
});

//Style for SCSS
gulp.task('scss', () => {
    return gulp.src(scssFile)
        .pipe(plumber((err) => {
            console.log(`SCSS task error: ${err.name}`);
            console.log(err.message);
        }))
        .pipe(sass({ style: 'compressed' }))
        .pipe(autoPrefixer())
        .pipe(rename({ suffix: '.min' }))
        .pipe(minifyCss())
        .pipe(gulp.dest(DistPathOfScss));
});

//Final concat of SCSS and css1rem
gulp.task('concatcss', () => {
    return gulp.src(finalCssPath).pipe(plumber((err) => {
            console.log(`CSS task error: ${err.name}`);
            console.log(err.message);
        }))
        .pipe(autoPrefixer())
        // .pipe(sourcemaps.init())
        .pipe(concat('main.css'))
        .pipe(minifyCss())
        // .pipe(sourcemaps.write())
        .pipe(gulp.dest('public/css'))
        .pipe(browserSync.reload({ stream: true }));;
});

//SCRIPTS for js
gulp.task('js', () => {
    var orderFileContent = fs.readFileSync(orderFile);
    var orderList = JSON.parse(orderFileContent).order;

    return gulp.src(scriptsPath)
        .pipe(plumber((err) => {
            console.log(`JS task error: ${err.name}`);
            console.log(err);
        }))
        .pipe(order(orderList))
        // .pipe(sourcemaps.init())
        .pipe(babel({
            presets: ['es2015']
        }))
        .pipe(concat('main.js'))
        .pipe(uglify())
        // .pipe(sourcemaps.write())
        .pipe(gulp.dest(DistPathOfJs));
});

//ES-LINT
gulp.task('lint', function() {
    return gulp.src(scriptsPath)
        .pipe(plumber((err) => {
            console.log(`LINT task error: ${err.name}`);
            console.log(err.message);
        }))
        .pipe(eslint({ configFile: '.eslintrc.json' }))
        .pipe(eslint.format());
});


//Image
gulp.task('image', () => {
    return gulp.src(imagePath)
        .pipe(imageMin([
            imageMin.gifsicle(),
            imageMin.jpegtran(),
            imageMin.optipng(),
            imageMin.svgo(),
        ]))
        .pipe(gulp.dest(DistPathOfImage));
});

//Fonts
gulp.task('font', () => {
    return gulp.src(fontPath)
        .pipe(gulp.dest(DistPathOfFont));
});

// we'd need a slight delay to reload browsers
// connected to browser-sync after restarting nodemon
var BROWSER_SYNC_RELOAD_DELAY = 1000;

gulp.task('nodemon', function (cb) {
  var called = false;
  return nodemon({

    // nodemon our expressjs server
    script: 'app.js',

    // watch core server file(s) that require server restart on change
    watch: ['app.js']
  })
    .on('start', function onStart() {
      // ensure start only got called once
      if (!called) { cb(); }
      called = true;
    })
    .on('restart', function onRestart() {
      // reload connected browsers after a slight delay
      setTimeout(function reload() {
        browserSync.reload({
          stream: false
        });
      }, BROWSER_SYNC_RELOAD_DELAY);
    });
});

gulp.task('browser-sync', ['nodemon'], () => {

  // for more browser-sync config options: http://www.browsersync.io/docs/options/
  browserSync({

    // informs browser-sync to proxy our expressjs app which would run at the following location
    proxy: 'http://localhost:3000',

    // informs browser-sync to use the following port for the proxied app
    // notice that the default port is 3000, which would clash with our expressjs
    port: 4001,

    // open the proxied app in chrome
    browser: ['chromium-browser']
  });
});


gulp.task('bs-reload', () => {
  browserSync.reload();
});

//Default
// gulp.task('default', ['js', 'css', 'scss', 'concatcss'], () => {
//     console.log('Starting ALL task');
// });'browser-sync'

//Watch
// gulp.task('watch', ['default'], () => {
//     livereload.listen();
//     console.log('Starting watch task ');
//     gulp.watch(scriptsPath, ['js']);
//     gulp.watch(cssPath, ['css']);
//     gulp.watch(scssPath, ['scss']);
//     gulp.watch(fontPath, ['font']);
//     gulp.watch(finalCssPath, ['concatcss']);
//     gulp.watch(imagePath, ['image']);
//     // gulp.watch(scriptsPath , ['lint'])
// });
gulp.task('watch', ['js', 'css', 'scss', 'concatcss', 'browser-sync'], () => {
  gulp.watch(scriptsPath, ['js', 'bs-reload']);
  gulp.watch(cssPath, ['css']);
  gulp.watch(scssPath, ['scss']);
  gulp.watch(finalCssPath, ['concatcss', 'bs-reload']);
  gulp.watch('views/**/*.handlebars', ['bs-reload']);
});
